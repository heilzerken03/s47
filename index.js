const txt_first_name = document.querySelector('#txt-first-name')
const txt_last_name = document.querySelector('#txt-last-name')
const span_full_name = document.querySelector('#span-full-name')

// keyup - event listener for typing in the input element
txt_first_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = (txt_first_name.value + " " +txt_last_name.value)

})
txt_last_name.addEventListener('keyup', () => {
	span_full_name.innerHTML = (txt_first_name.value + " " + txt_last_name.value)

})

txt_first_name.addEventListener('keyup',(event) => {
	console.log(event.target)
	console.log(event.target.value)
})

txt_last_name.addEventListener('keyup',(event) => {
	console.log(event.target)
	console.log(event.target.value)
})

// span_full_name.addEventListener('keyup', (event) => {
// 	console.log(txt_first_name)
// 	console.log(txt_last_name)
// 	console.log(txt_first_name.value)
// 	console.log(txt_last_name.value)

// 	// event.target is just the same as the current element which means this code below will also work the same way.
// 	// console.log(txt_first_name)
// 	// console.log(txt_first_name.value)

// 	// console.log(event.target)
//  //    console.log(event.target.value)
// })

